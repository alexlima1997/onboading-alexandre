(function ($) {

  var exports = {};

  var anchor = function () {
    $('.btn-start').click(function (e) {
      $('html, body').animate({
        scrollTop: $('main').offset().top
      }, 1000);
    });
  };

  var btnPodcast = function(){
    $('.btn-podcast').click(function(){
      $(this).parent().toggleClass('is-active').find('.transcription').slideToggle();
      return false;
    })
  }

  var init = function () {
    anchor();
    btnPodcast();
  }();

  return exports;

})(jQuery);