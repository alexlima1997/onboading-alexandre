(function ($) {
  
  var exports = {};

  var anchor = function () {
    $('.btn-start').click(function (e) {
      $('html, body').animate({
        scrollTop: $('main').offset().top
      }, 1000);
    });
  };

  var btnPodcast = function(){
    $('.btn-podcast').click(function(){
      $(this).closest('.podcast').toggleClass('is-active').find('.transcription').slideToggle();
      return false;
    })
  }

  var dropside = function () {
    $('.dropside-open').on('click', function () {
      $(this).closest('.dropside').toggleClass('is-active');
    });
  };

  $('.btn-show-content').click(function(){
    setTimeout(
      function() {
        if($('.question-discursive .question-feedback').is(":visible")) {
          $('.content-hidden').slideDown();
        }
      }, 300);
    });
  
  var init = function () {
    anchor();
    btnPodcast();
    dropside();
  }();
  
  return exports;
  
})(jQuery);